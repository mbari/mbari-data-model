# MBARI Data Models

This work was done to try and create a generalized data model that could create links to the various legacy data systems and serve as a foundation for the integration layer of data management at MBARI.

## Model matrices

### Physical, digital, conceptual, or other kinds of things

| System | Class/Table | Identifier | Timestamp of start | Timestamp of end | General name/value pairs |
|:--------:|:-----------:|:----------:|:------------------:|:----------------:|:------------------------:|
| Model | Resource | uuid | | | |
| STOQS | MeasuredParameter + Measurement (through MeasuredParameterResource and linked to Parameter) and SampledParameter + Sample  | id | link to InstantPoint (no start or end) | link to InstantPoint (no start or end) | N/A |
| EXPDB | | | | | |
| SamplesDB | | | | | |
| VARS | | | | | |
| SSDS | DataContainer / Resource | id | startDate | endDate | Keyword(with assoc table) sort of approximates this, but single thing like a tag |
| BOG | | | | | |
| ODSS | | | | | |
| LRAUV | | | | | |
| WGMS | | | | | |
| ESP | | | | | |
| Floats | | | | | |
| W3C PROV | Entity (through SampledParameterResource and linked to Parameter) | id | no variable, could be attribute | no variable, could be attribute | attributes |
| CSW | | | | | |
| Unidata CDM | | | | | |
| ERDDAP | EDD (EDDTable / EDDGrid) | | | | |
| Geoserver | | | | | |
| Hyrax | | | | | |
| THREDDS | | | | | |

Questions:

1. For STOQS, what are activity parameters?

### Moment interval of time where something happens

This is also how entities come into existence and how their attributes change to become new entities, often making use of previously existing entities to achieve this

| System | Class/Table | Identifier | Timestamp of start | Timestamp of end | General name/value pairs |
|:--------:|:-----------:|:----------:|:------------------:|:----------------:|:------------------------:|
| Model | Activity | | | | |
| STOQS | Activity/Campaign  | id | startDate | endDate | N/A |
| EXPDB | | | | | |
| SamplesDB | | | | | |
| VARS | | | | | |
| SSDS | DataProducer | id | startDate | endDate | Keyword(with assoc table) sort of approximates this, but single thing like a tag |
| BOG | | | | | |
| ODSS | | | | | |
| LRAUV | | | | | |
| WGMS | | | | | |
| ESP | | | | | |
| Floats | | | | | |
| W3C PROV | Activity | id | startTime | endTime | attributes |
| CSW | | | | | |
| Unidata CDM | | | | | |
| ERDDAP | | | | | |
| Geoserver | | | | | |
| Hyrax | | | | | |
| THREDDS | | | | | |

### Entity responsible and/or associated with something

| System | Class/Table | Identifier | General name value pairs |
|:--------:|:-----------:|:----------:|:------------------------:|
| Model | | | |
| STOQS | Platform | id | N/A |
| EXPDB | | | |
| SamplesDB | | | |
| VARS | | | |
| SSDS | Device/Software + Person | id | N/A |
| BOG | | | |
| ODSS | | | |
| LRAUV | | | |
| WGMS | | | |
| ESP | | | |
| Floats | | | |
| W3C PROV | Agent | id | attributes |
| CSW | | | |
| Unidata CDM | | | |
| ERDDAP | | | |
| Geoserver | | | |
| Hyrax | | | |
| THREDDS | | | |

### Provenance

| System | Generation of Things | Responsibility for generation and/or thing (implies partial or full responsibility) | Lineage of Things | Lineage of Generation | Sequence of generation |
|:--------:|:-----------:|:----------:|:------------------:|:----------------:|:------------------------:|
| Model | | | | | |
| STOQS | Activity has no knowledge of MeasuredParameters, MeasureParameterResource knows about it's Activity and it's MeasuredParameter so it is the linkage between Activity and MeasuredParameter (same for Sample) | Platform that is linked through Activity to MeasuredParameter | N/A (no parent child relation for measured parameters, but there is for Samples) |  N/A (no parent child relation for activity) | N/A |
| EXPDB | | | | | |
| SamplesDB | | | | | |
| VARS | | | | | |
| SSDS | DataProducer->DataContainer (two way: DataContainer has DataProducerID_FK, DataProducer has Assoc table for Resources, and Assoc table for DataContainers as inputs, but evidently no knowledge of outputs) | Device or Software that is linked to the DataProducer which produced the DataContainer as an output | No direct connection, would have to derive through DataProducers | DataProducer has childDataProducers (tracked on DataProducer as ParentID_FK) | Don't really have this distinction as there is just a tree of DataProducers and DataContainers, no window over those to define a specific, self-contained sequence of steps |
| BOG | | | | | |
| ODSS | | | | | |
| LRAUV | | | | | |
| WGMS | | | | | |
| ESP | | | | | |
| Floats | | | | | |
| W3C PROV | Activity->Entity (Entity tracks as 'wasGeneratedBy', Activity tracks inputs as 'used' but does not seem to track outputs). Note that each relationship has name:value 'attributes' associated with them | Entity->Agent, Activity->Agent (Activity tracks as 'wasAssociatedWith', Entity tracks as 'wasAttributedTo'). 'Roles' also exist to describe linkage. Note that each relationship has name:value 'attributes' associated with them | Entity->Enitity (tracked as 'wasDerivedFrom'). Note that each relationship has name:value 'attributes' associated with them | Activity->Activity (tracked as 'wasInformedBy)'. Note that each relationship has name:value 'attributes' associated with them | Plan |
| CSW | | | | | |
| Unidata CDM | | | | | |
| ERDDAP | | | | | |
| Geoserver | | | | | |
| Hyrax | | | | | |
| THREDDS | | | | | |

Questions:

1. STOQS: does ActivityParameter along with AnalysisMethod give some kind of processing provenance tracking? Or is that the Resource?
1. STOQS: What is the role of the Resource?
1. STOQS: What is the ActivityParameter and does that relate to Provenance at all?

## Model Diagrams

### Model

Model Diagram - Under construction

### STOQS

Core concept diagram:

![STOQS](imgs/stoqs.jpg)

### EXPDB

![EXPDB Diagram](imgs/expd.jpg)

### SamplesDB

![SamplesDB Diagram](imgs/samplesDB.jpg)

### VARS

![VARS Diagram](imgs/vars.jpg)

### SSDS

![SSDS Core](imgs/ssds.jpg)

### BOG

![BOG Diagram](imgs/bog.jpg)

### ODSS

Under construction

### LRAUV

Under construction

### WGMS

Under construction

### ESP

Under construction

### Floats

Under construction

### W3C PROV

![W3C PROV Model](https://www.w3.org/TR/2013/NOTE-prov-primer-20130430/images/key-concepts.png)

![W3C PROV DM](https://www.w3.org/TR/2013/REC-prov-dm-20130430/uml/component1.png)

![W3C PROV DM2](https://www.w3.org/TR/2013/REC-prov-dm-20130430/uml/component3.png)

![W3C PROV DM3](https://www.w3.org/TR/2013/REC-prov-dm-20130430/uml/Component3b.png)

### CSW

### Unidata CDM

![Uniddata CDM](https://docs.unidata.ucar.edu/netcdf-java/5.2/userguide/images/netcdf-java/reference/uml/CDM-UML.png)
