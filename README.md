# README

This repository contains the modeling work for for a core set of objects and classes used in systems at MBARI.  The goal is to create high level models to allow for a combined catalog of various MBARI data.

## Development Requirements

1. Java Developers Kit (JDK 8+)
1. Maven
1. Protocol Buffers (the compiler) - note, the version you install must be compatible with the version in the pom.xml file (3.14.0 as of this writing)

## Compiling code

If you want to compile the protocol buffer definitions into supporting libraries for various languages, run

```bash
mvn compile
```

at the command line and that will compile the protocol buffer definition files (in src/main/protocol-buffer/*.proto) to library source code for the following languages:

1. C++ (in generated/src/main/cpp)
1. C# (in generated/src/main/csharp)
1. Java (in generated/src/main/java)
1. JavaScript (in generated/src/main/js)
1. ObjectiveC (in generated/src/main/objc)
1. PHP (in generated/src/main/php)
1. Python (in generated/src/main/python)
1. Ruby (in generated/src/main/ruby)

Maven will then compile the code in generated/src/main/java into .class files located in target/classes

## Installing in Local Maven Repo

To install this package locally, after checking out from the Git repo, run:

```bash
mvn install
```

and a jar file will be created in the target directory and installed locally so you can use it your projects.

## Details

The models are actually defined in a protocol buffers definition file in the src/main/protocol-buffer directory in a series of .proto files.  From these files, the maven build will actually generate class definitions in Java, C++, C#, JavaScript, ObjectiveC, PHP, Python and Ruby languages and will place those files in their respective directories under a generated/src directory.  The maven build then generates a JAR file for use in Java projects and installs that jar in your local maven repo.

## Deploy Changes

This code is the foundation for many other pieces of software and when changes are made, certain steps need to be taken when deploying those changes.  When you are happy with the changes to the protocol buffer definitions, there are several things that need to happen.

1. First, you need to decide how large the changes are so that you can create the proper version number for release.  If it's a bug fix, it often necessitates a bump of the third number in the version number.  If it's a non-breaking change that alters the capability of the code, but does not break existing clients, you can bump the second number and if it's a breaking and/or major change, you will want to increment the first number in the version.
1. Once the version number has been decided, you need to edit the pom.xml file and update the version number (around line 9) to the number you are ready to release.
1. Then, in this file (README.md), add a line at the bottom of the file with the version number and the changes that were made in this version.
1. Now you are ready to check the source code in to the Git repo.  First commit these changes to your local repo.  Then create a tag with the version number that you have assigned to these changes.  Push the head and the tag to the remote repository.
1. Now you are ready to deploy the updated maven artifact to the MBARI GitHub maven repository.  If you have permissions and the correct setup in your Maven ~/.m2/settings.xml file, you can deploy a version of this using:

```bash
mvn deploy
```

1. Thi will push version to the Maven repository located [here](https://github.com/mbari-org/maven/packages/641405). This will make it available to other projects who can include them in their pom files.
1. Lastly, go to the GitHub page and replace the Release notes with the contents the Version History below

## TODO

Figure out how to build libraries out of the protocol buffer generated source that is located in the 'generated' folder for the languages other than Java.

## Version History

1.1.1 - Added the crc to the File object so that we can store the CRC value associated with a File.

1.1.2 - Added string field 'content_tree' to the Resource object to hold the string representation of the content of the file.  For example, you could convert an XML document to JSON and then store the JSON in string format in this field.

2.0.0 - Breaking change where I removed the checksum value on file and reorder properties file.crc and resource.content_tree so they are in alphabetical order.  This changes the underlying protocol structure so it's a breaking change

2.0.1 - Upgraded the Protocol buffer libraries from 3.6.1 to 3.10.0
2.0.2 - Upgraded the Protocol buffer librarres from 3.10.0 to 3.14.0
